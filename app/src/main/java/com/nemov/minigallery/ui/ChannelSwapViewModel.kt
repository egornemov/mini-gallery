package com.nemov.minigallery.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Matrix
import android.media.ExifInterface
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.File


interface ChannelSwapController {
    fun editedImage(): LiveData<Bitmap?>
    fun updateSource(path: String)
    fun doChannelSwap()
}

class ChannelSwapViewModel : ViewModel(), ChannelSwapController {

    private var image = MutableLiveData<Bitmap?>().apply {
        value = null
    }

    private lateinit var originalBitmap: Bitmap
    private lateinit var swappedBitmap: Bitmap

    override fun editedImage(): LiveData<Bitmap?> = image

    override fun updateSource(path: String) {
        if (!this@ChannelSwapViewModel::originalBitmap.isInitialized) {
            GlobalScope.launch {
                val file = File(path)
                if (file.exists()){
                    val bitmap = BitmapFactory.decodeFile(file.absolutePath)

                    val exif = ExifInterface(file.absoluteFile.toString())
                    val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                    val matrix = Matrix()

                    when(orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90F)
                        ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180F)
                        ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270F)
                    }

                    originalBitmap = Bitmap.createBitmap(bitmap, 0,0 , bitmap.width, bitmap.height, matrix, true)
                    when(orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> bitmap.recycle()
                        ExifInterface.ORIENTATION_ROTATE_180 -> bitmap.recycle()
                        ExifInterface.ORIENTATION_ROTATE_270 -> bitmap.recycle()
                    }
                    image.postValue(originalBitmap)
                }
            }
        }
    }

    override fun doChannelSwap() {
        GlobalScope.launch {
            if (!this@ChannelSwapViewModel::swappedBitmap.isInitialized) {
                swappedBitmap = makeSwappedBitmap(originalBitmap)
            }
            image.postValue(
                if (image.value === swappedBitmap) originalBitmap
                else swappedBitmap
            )
        }
    }

    private suspend fun makeSwappedBitmap(src: Bitmap): Bitmap {
        return GlobalScope.async {
            val width: Int = src.width
            val height: Int = src.height

            val pixels = IntArray(width * height)
            src.getPixels(pixels, 0, width, 0, 0, width, height)

            val finalArray = IntArray(width * height)

            for (i in finalArray.indices) {
                val red: Int = Color.red(pixels[i])
                val green: Int = Color.green(pixels[i])
                val blue: Int = Color.blue(pixels[i])
                finalArray[i] = Color.rgb(green, red, blue)
            }
            val dst = Bitmap.createBitmap(finalArray, width, height, Bitmap.Config.ARGB_8888)

            return@async dst
        }.await()
    }
}