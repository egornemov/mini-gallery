## TODO

* Handle states (loading, failure)
* Fix naming
* Fix lint issues
* Add placeholder for corrupted path
* Inject coroutine scopes to ViewModels
* Add Model layer
